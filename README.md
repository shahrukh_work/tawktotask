# TawkToTask

## TawkToTask Swift iOS app  getting started.

**Built using XCode 10.5 (Swift 5)**


## How to Build?

1. Clone this repo
2. Open shell window and navigate to project folder
3. Run pod install
4. Open TawkToTask.xcodeproj and run the project on selected device or simulator


## Folder Heirarcy
1. AppDelegate
2. Constant
3. DataHandler
4. CoreDataManager
5. Networking
6. Models
7. Class
8. Utility
9. Extensions


## Architecture Explaination

### Controllers
1. GitUsersViewController
    1. This contains the code for showing git users from api to screen. 
    2. It contains a tableview only to show the list of users.
    3. Pagination is implemented to load more users. 
    4. Three cell are showing based on conditions

### Conditions 
1. If users has saved note in coredata it will start to show note button in the cell
2. Invert colora of image after downloading the image from url.
    
2. ProfileViewController
    1. This screen for load data of particular user after providing user name. 
    2. you can save note locally to show.
    
### ViewModels

----> Controllers <-----
1. GitUsersViewModel
2. ProfileViewModel

----> Cells <-----
1. NormalViewModel
2. NoteViewModel
3. InvertedViewModel


## ViewModel Functionality


### GitUsersViewModel
1. Get Data from Api
2. Get data drom Coredata


### ProfileViewModel
1. Get Data from Api
2. Get data drom Coredata
3. Save Data in coredata


### All Cells ViewModels
1. Used to get data from Api Call and config the data with UItableViewCell


# CoreData

## Files
1. GitHubUsersList.xcdatamodelId
2. CoreDataManager
3. Datahandler

### GitHubUsersList.xcdatamodelId
1. It contains the db schema

### CoreDataManager 
1. Create Manager Objected Context using persistentStoreCoodinator

### DataManager
1. Contains all the logic for coredata
2. Contains Create, Get, Update Logic



# Unit Testing / Automation Testing

## Files
1. GitHubUsersListCoreData
2. NetworkTestCase


### GitHubUsersListCoreData
1. Contains all the test cases for Coredata Create, Get, Update


### NetworkTestCase
1. Contains apis call for both GetUsersList and GetUser
2. It is also test the parsed data into the model from the api.
