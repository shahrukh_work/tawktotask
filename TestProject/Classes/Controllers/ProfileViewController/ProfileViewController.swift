//
//  ProfileViewController.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import UIKit

class ProfileViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var organizationLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var blogLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    
    //MARK: - Variables
    var viewModel: ProfileViewModel?
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    //MARK: - Setup
    func setup () {
        
        textView.text = viewModel?.user?.note ?? ""
        if (viewModel?.user?.note ?? "") == "" {
            textView.text = "Type note here. . ."
            textView.textColor = UIColor.lightGray
            textView.delegate = self
        }
        loadData(userName: viewModel?.user?.login ?? "")
    }
    
    
    //MARK: - Actions
    @IBAction func savePressed(_ sender: Any) {
        //Saving note is coredata
        if textView.text != "" && textView.text! != "Type note here. . ." {
            viewModel?.updateNote(note: textView.text ?? "", userId: viewModel?.user?.login ?? "")
            self.showOkAlert("You note has been saved successfully.")
            return
        }
        self.showOkAlert("Please write something in the note.")
    }
    
    
    //MARK: - Private Methods
    func loadData (userName: String) {
        
        viewModel?.getUser (userName: userName) {[weak self] in
            
            self?.blogLabel.text = self?.viewModel?.user?.blog ?? ""
            self?.nameLabel.text = self?.viewModel?.user?.name ?? ""
            self?.organizationLabel.text = self?.viewModel?.user?.company ?? ""
            self?.profileImageView.imageFromServerURL(urlString:  self?.viewModel?.user?.avatar_url ?? "", placeHolderImage: #imageLiteral(resourceName: "profile")){}
            
        } failure: { (error) in
            Utility.showAlert(message: error?.localizedDescription ?? "", viewController: self)
        }

    }
}


//MARK: - UITextViewDelegate
extension ProfileViewController: UITextViewDelegate {
    //Place oholder for textview
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type note here. . ."
            textView.textColor = UIColor.lightGray
        }
    }
}
