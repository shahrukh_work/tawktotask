//
//  ProfileViewModel.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import Foundation


class ProfileViewModel {
    
    var user: User?
    
    
    init(user: User) {
        self.user = user
    }
    //MARK: - APICALl
    func getUser (userName: String, success: @escaping () -> (), failure: @escaping (_ error: Error?) -> ()) {
        
        var params: [String: Any] = [:]
        params[""] = userName
        
        ApiHandler.shared.sendRequest(ApiRoutes.userList, type: .queryString, parameters: params) { [weak self] (result, error, response) in
                
            if error == nil {
                
                let decoder = JSONDecoder()
                
                if let parsedData = try? decoder.decode(User.self, from: result!) {
                    self?.user = parsedData
                    
                    DispatchQueue.main.async {
                        success()
                    }
                }
                
            } else {
                
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
    }
    
    func updateNote (note: String, userId: String) {
        DataHandler().updateCacheUsers(userId: userId, note: note)
    }
}
