//
//  GitUsersViewController.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import UIKit

class GitUsersViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    
    //MARK: - Variables
    let spinner = UIActivityIndicatorView(style: .gray)
    let viewModel = GitUsersViewModel()
    var lastLoadedOffet = 0
    var filtersUsers = [User]()
    var isFilter = false
    var noteVM: NoteViewModel?
    var invertedVM: InvertedViewModel?
    var normalVM: NormalViewModel?
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.setAllNotes()
        tableView.reloadData()
    }
    
    //MARK: - Setup
    func setup () {
        searchTextField.setLeftPaddingPoints(16)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchTextField.delegate = self
        
        NetWorkCheck.shared.monitor.pathUpdateHandler = { [self] path in
            self.loadData(offset: self.lastLoadedOffet)
            print(path.isExpensive)
        }
    }
    
    
    //MARK: - Actions
    
    
    //MARK: - Private Methods
    func loadData (offset: Int) {
        
        viewModel.getUsers (offset: lastLoadedOffet) {
            self.lastLoadedOffet = self.viewModel.users.last?.id ?? 0
            self.assignViewModelsData()
            self.tableView.reloadData()
            
        } failure: { (error) in
            self.assignViewModelsData()
            self.tableView.reloadData()
        }
    }
    
    //This function is used to show spinner in the footer
    func spinner (tableView: UITableView, indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
           // print("this is the last cell")
            if !isFilter {
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                self.tableView.tableFooterView = spinner
                self.tableView.tableFooterView?.isHidden = false
            }
        }
    }
    
    //Every cell has is own view model and assign data source to view models
    func assignViewModelsData () {
        self.noteVM = NoteViewModel(dataSource: self.viewModel.users)
        self.invertedVM = InvertedViewModel(dataSource: self.viewModel.users)
        self.normalVM = NormalViewModel(dataSource: self.viewModel.users)
    }
}


//MARK: - UITextFieldDelegate
extension GitUsersViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //This check is used the user is removing the text from the search bar
        if (textField.text!.count == 1) && (string).isEmpty {
            isFilter = false
            self.assignViewModelsData()
            tableView.reloadData()
            return true
        }
        
        spinner.isHidden = true
        isFilter = true
        
        //Filtering data based of user name and Url below that as there is no description in the api
        filtersUsers = viewModel.users.filter({ (user) -> Bool in
            
            if (user.login.lowercased()).contains((textField.text! + string).lowercased()) || (user.note.lowercased()).contains((textField.text! + string).lowercased()) {
                return true
            }
            return false
        })
        
        
        //Assinging filtered data source to Cell view models
        self.invertedVM = InvertedViewModel(dataSource: filtersUsers)
        self.noteVM = NoteViewModel(dataSource: filtersUsers)
        self.normalVM = NormalViewModel(dataSource: filtersUsers)
        tableView.reloadData()
        return true
    }
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension GitUsersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFilter {
            return filtersUsers.count
        }
        return viewModel.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        if viewModel.users[indexPath.row].isNoteAvailable {
            
            //every 3 cell image should inverted
            if indexPath.row % 4 == 0 {
                return invertedVM!.config(tableView: tableView, indexPath: indexPath)
                
            } else {
                return noteVM!.config(tableView: tableView, indexPath: indexPath)
            }
            
        } else {
            
            if indexPath.row % 4 == 0 {
                return invertedVM!.config(tableView: tableView, indexPath: indexPath)
                
            } else {
                return normalVM!.config(tableView: tableView, indexPath: indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userListCoordinator = Coordinator(navigation: self.navigationController!)
        
        if isFilter {
            userListCoordinator.showUserDetail(dataSource: filtersUsers[indexPath.row], isFilter: true)
            return
        }
        userListCoordinator.showUserDetail(dataSource: viewModel.users[indexPath.row], isFilter: false)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.spinner(tableView: tableView, indexPath: indexPath)
    }
}


// MARK: - UIScrollViewDelegate
extension GitUsersViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //This used for pagination it detect the when the user scroll down to the bottom of the table view
        if scrollView == tableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                lastLoadedOffet += lastLoadedOffet
                self.loadData(offset: self.lastLoadedOffet)
            }
        }
    }
}
