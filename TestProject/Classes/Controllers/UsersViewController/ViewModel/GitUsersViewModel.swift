//
//  UsersViewModel.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import Foundation


class GitUsersViewModel {
    
    var users: [User] = []    
    
    
    //MARK: - APICALl
    func getUsers (offset: Int, success: @escaping () -> (), failure: @escaping (_ error: Error?) -> ()) {
        
        var params: [String: Any] = [:]
        params["since"] = offset
        //API Call
        ApiHandler.shared.sendRequest(ApiRoutes.userList, parameters: params) { [weak self] (result, error, response) in
                
            if error == nil {
                
                let decoder = JSONDecoder()
                
                //Parsing data if received from api
                if let parsedData = try? decoder.decode([User].self, from: result!) {
                    
                    if NetWorkCheck.shared.lastState == .offline {
                        self?.users.removeAll()
                    }
                    
                    NetWorkCheck.shared.lastState = .online //Saving internet state
                    
                    self?.cacheUsers(cacheUsers: parsedData) //Locally saving data
                    self?.users.append(contentsOf: parsedData) // appening new data
                    self?.setAllNotes() //Setting if note is saved with api user
                    DispatchQueue.main.async {
                        success()
                    }
                }
                
            } else {
                
                NetWorkCheck.shared.lastState = .offline //Saving internet state
                self?.users = DataHandler().getUsersData() //getting data from local
                self?.setAllNotes() //Setting if note is saved with api user
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
    }
    
    func cacheUsers (cacheUsers: [User]) {
        //Saving user api record it will be used internet is not available
        DataHandler().cacheUsers(users: cacheUsers)
    }
    
    func setAllNotes () {
        //Gettting data from coredata if the user has saved any notes in coredata
        self.users = DataHandler().setAllNotesVariable(users: self.users)
    }
}
