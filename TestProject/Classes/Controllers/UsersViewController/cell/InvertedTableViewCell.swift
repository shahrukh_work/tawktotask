//
//  InvertedTableViewCell.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import UIKit

class InvertedTableViewCell: UITableViewCell, UserProtocol {
    
   
    
    //MARK: - Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
 
    
    //MARK: - Variables
    var indexPath: IndexPath?    
    
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure (data: UserDataModel) {

        userNameLabel.text = data.login
        descriptionLabel.text = data.organizations_url
        
        ApiHandler.shared.imageFromServerURL(urlString: data.avatar_url, placeHolderImage: UIImage(named: "profile")!) { (image) in
            
            Utility.inversion(image: image) { (image) in
                DispatchQueue.main.async {
                    self.userImageView.image = image
                    
                }
            }
        }
    }
}
