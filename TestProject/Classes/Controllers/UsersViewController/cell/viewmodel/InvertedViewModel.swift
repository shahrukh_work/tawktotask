//
//  CellViewModel.swift
//  TestProject
//
//  Created by MAC on 17/11/2021.
//

import Foundation
import UIKit

class InvertedViewModel: UserViewModel {
    var dataSource: [UserDataModel] = []
    
    func config(tableView: UITableView, indexPath: IndexPath) -> UserProtocol {
        let cell = tableView.register(InvertedTableViewCell.self, indexPath: indexPath)
        cell.configure(data: dataSource[indexPath.row])
        return cell
    }
    
    init(dataSource: [UserDataModel]) {
        self.dataSource = dataSource
    }
    
}
