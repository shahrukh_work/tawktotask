//
//  CellViewModel.swift
//  TestProject
//
//  Created by MAC on 17/11/2021.
//

import Foundation
import UIKit

protocol UserViewModel {
    func config(tableView: UITableView, indexPath: IndexPath) -> UserProtocol
}

protocol UserProtocol: UITableViewCell {
    func configure (data: UserDataModel)
}

protocol UserDataModel {
    var id: Int {get set}
    var organizations_url  : String {get set}
    var received_events_url  : String {get set}
    var following_url  : String {get set}
    var login  : String {get set}
    var avatar_url  : String {get set}
    var url  : String {get set}
    var node_id  : String {get set}
    var subscriptions_url  : String {get set}
    var repos_url  : String {get set}
    var type : String {get set}
    var html_url : String {get set}
    var events_url  : String {get set}
    var site_admin : Bool {get set}
    var starred_url  : String {get set}
    var gists_url  : String {get set}
    var gravatar_id  : String {get set}
    var followers_url  : String {get set}
    var note : String {get set}
    var name: String {get set}
    var company: String {get set}
    var blog: String {get set}
    var isNoteAvailable : Bool {get set}
    var profileImage: UIImage {get set}
    var invertedImage: UIImage {get set}
}

class NoteViewModel: UserViewModel {
    var dataSource: [UserDataModel] = []
    
    func config(tableView: UITableView, indexPath: IndexPath) -> UserProtocol {
        let cell = tableView.register(NoteTableViewCell.self, indexPath: indexPath)
        cell.configure(data: dataSource[indexPath.row])
        return cell
    }
    
    init(dataSource: [UserDataModel]) {
        self.dataSource = dataSource
    }
}
