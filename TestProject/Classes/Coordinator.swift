//
//  Coordinator.swift
//  TestProject
//
//  Created by MAC on 18/11/2021.
//

import Foundation
import UIKit

protocol UsersListsCoordinator {
    var navigation: UINavigationController {get set}
    func showUserDetail (dataSource: User, isFilter: Bool)
}

class Coordinator: UsersListsCoordinator {
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func showUserDetail (dataSource: User, isFilter: Bool) {
        
        let vc = ProfileViewController()
        let vm: ProfileViewModel?
        
        if isFilter {
            vm = ProfileViewModel(user: dataSource)
            vc.viewModel = vm
            self.navigation.pushViewController(vc, animated: true)
            return
        }
        vm = ProfileViewModel(user: dataSource)
        vc.viewModel = vm
        self.navigation.pushViewController(vc, animated: true)
    }
}
