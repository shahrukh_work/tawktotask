//
//  Constants.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import Foundation
import Network

enum LastStateOfNetwork {
    case online
    case offline
}

let kApplicationWindow = Utility.getAppDelegate()!.window

class NetWorkCheck {
    
    static var shared = NetWorkCheck()
    var lastState: LastStateOfNetwork = .online
    let queue = DispatchQueue(label: "Monitor")
    let monitor = NWPathMonitor()
    
    init() {
        monitor.start(queue: queue)
    }

}

struct ApiRoutes {
    
    static let baseUrl = ""
    static let userList = "https://api.github.com/users"
}
