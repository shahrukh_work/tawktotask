//
//  DataHandler.swift
//  TestProject
//
//  Created by MAC on 17/11/2021.
//

import Foundation
import CoreData
import UIKit

public class DataHandler {
    
    let context:NSManagedObjectContext
    
    init () {
        context = CoreDataManager.sharedInstance.managedObjectContext
    }
    
    
    func cacheUsers (users: [User]) {
        
        for newUser in users { //Checking if same data already exist while caching
            let recordExist = alreadyExist(userId: newUser.login)
            if recordExist > 0 {
                return
            }
            
            //Checking if does not already exist data trying to save it in coredata
            let user = CacheUsers(context: context)
            user.name = newUser.login
            user.descriptionText = newUser.followers_url
            user.imagePath = newUser.avatar_url
            user.note = newUser.note
            
            DispatchQueue.global().sync {
                //Downloading the image in coredata and saving it serial wise
                Utility.downloadImage(urlString: newUser.avatar_url , { (data) in
                    user.imageData = data
                    
                    DispatchQueue.main.async {
                        //After downloading save the context
                        CoreDataManager.sharedInstance.saveContext()
                    }
                })
            }

        }
    }
    
    
    func alreadyExist(userId: String) -> Int {
        var users: [CacheUsers] = []
        do{
            let fetchRequest:NSFetchRequest<CacheUsers> = CacheUsers.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "name == %@", userId)
            users = try context.fetch(fetchRequest)
            return users.count
            
        } catch {

        }
        return 0
    }
    
    // saving the note against particular user
    func updateCacheUsers(userId: String, note: String) {
        var users: [CacheUsers] = []
        do{
            let fetchRequest:NSFetchRequest<CacheUsers> = CacheUsers.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "name == %@", userId)
            users = try context.fetch(fetchRequest)

            for user in users {
                user.note = note
                user.isNoteAvailable = true
            }
        } catch {

        }
        CoreDataManager.sharedInstance.saveContext()

    }
    
    //Assign the user note property to show it on the list
    func setAllNotesVariable (users: [User]) -> [User] {
        var allUsers = users
        for (index, value) in users.enumerated() {
            var cacheUser: [CacheUsers] = []
            do{
                let fetchRequest:NSFetchRequest<CacheUsers> = CacheUsers.fetchRequest()
                fetchRequest.predicate = NSPredicate(format: "name == %@", value.login)
                cacheUser = try context.fetch(fetchRequest)
               
                for cache in cacheUser {
                    
                     if ((cache.isNoteAvailable as? Bool)!) {
                        allUsers[index].isNoteAvailable = true
                        allUsers[index].note = cache.note ?? ""
                    }
                }
    
            } catch {
                
            }
        }

        return allUsers
    }
    
    //getting all user data from coredata
    func getUsersData() -> [User] {
        var clientData: [CacheUsers] = []

        var client : [User] = []
        do {
            let fetchRequest:NSFetchRequest<CacheUsers> = CacheUsers.fetchRequest()
        
            clientData = try context.fetch(fetchRequest)
            
            for item in clientData {
                var clientObj = User()
                clientObj.login = item.name!
                clientObj.organizations_url = item.descriptionText!
                clientObj.avatar_url = item.imagePath!
                clientObj.isNoteAvailable = item.isNoteAvailable
                clientObj.note = item.note!
                clientObj.profileImage = UIImage(data: item.imageData ?? (UIImage(named: "profile")?.pngData())!)!
                client.append(clientObj)
            }

        } catch {

        }
        return client
    }

}
    
