//
//  User.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import Foundation
import UIKit


struct User : Codable, UserDataModel {

    
    var id : Int = 0
    var organizations_url  = ""
    var received_events_url  = ""
    var following_url  = ""
    var login  = ""
    var avatar_url  = ""
    var url  = ""
    var node_id  = ""
    var subscriptions_url  = ""
    var repos_url  = ""
    var type  = ""
    var html_url  = ""
    var events_url  = ""
    var site_admin : Bool = false
    var starred_url  = ""
    var gists_url  = ""
    var gravatar_id  = ""
    var followers_url  = ""
    var note = ""
    var name: String = ""
    var company: String = ""
    var blog: String = ""
    var isNoteAvailable = false
    var profileImage: UIImage = UIImage(named: "profile")!
    var invertedImage: UIImage = UIImage(named: "profile")!

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case organizations_url = "organizations_url"
        case received_events_url = "received_events_url"
        case following_url = "following_url"
        case login = "login"
        case avatar_url = "avatar_url"
        case url = "url"
        case node_id = "node_id"
        case subscriptions_url = "subscriptions_url"
        case repos_url = "repos_url"
        case type = "type"
        case html_url = "html_url"
        case events_url = "events_url"
        case site_admin = "site_admin"
        case starred_url = "starred_url"
        case gists_url = "gists_url"
        case gravatar_id = "gravatar_id"
        case followers_url = "followers_url"
        case note = "note"
        case name = "name"
        case company = "company"
        case blog = "blog"
        case isNoteAvailable = "isNoteAvailable"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        organizations_url = try values.decodeIfPresent(String.self, forKey: .organizations_url)!
        received_events_url = try values.decodeIfPresent(String.self, forKey: .received_events_url)!
        following_url = try values.decodeIfPresent(String.self, forKey: .following_url)!
        login = try values.decodeIfPresent(String.self, forKey: .login)!
        avatar_url = try values.decodeIfPresent(String.self, forKey: .avatar_url)!
        url = try values.decodeIfPresent(String.self, forKey: .url)!
        node_id = try values.decodeIfPresent(String.self, forKey: .node_id)!
        subscriptions_url = try values.decodeIfPresent(String.self, forKey: .subscriptions_url)!
        repos_url = try values.decodeIfPresent(String.self, forKey: .repos_url)!
        type = try values.decodeIfPresent(String.self, forKey: .type)!
        html_url = try values.decodeIfPresent(String.self, forKey: .html_url)!
        events_url = try values.decodeIfPresent(String.self, forKey: .events_url)!
        site_admin = try values.decodeIfPresent(Bool.self, forKey: .site_admin) ?? false
        starred_url = try values.decodeIfPresent(String.self, forKey: .starred_url)!
        gists_url = try values.decodeIfPresent(String.self, forKey: .gists_url)!
        gravatar_id = try values.decodeIfPresent(String.self, forKey: .gravatar_id)!
        followers_url = try values.decodeIfPresent(String.self, forKey: .followers_url)!
        note = try values.decodeIfPresent(String.self, forKey: .note) ?? ""
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        company = try values.decodeIfPresent(String.self, forKey: .company) ?? ""
        blog = try values.decodeIfPresent(String.self, forKey: .blog) ?? ""
        //isNoteAvailable = try values.decodeIfPresent(Bool.self, forKey: .isNoteAvailable)!
    }
    
    init() {
        
    }
}
