//
//  ApiHandler.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import Foundation
import UIKit


enum ApiRequestType: String {
    case queryString
    case urlQueryItem
}
enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    case update = "UPDATE"
    case delete = "DELETE"
}

class ApiHandler {
    
    static var shared = ApiHandler()
    let session = URLSession.shared
    
    func sendRequest (_ url: String, _ httpMethod: HttpMethod = .get, type: ApiRequestType = .urlQueryItem, parameters: [String: Any]? ,_ completionHandler: @escaping (_ data: Data?, _ error: Error? , _ response: URLResponse?) -> ()) {
        
        var request: URLRequest?

        if type == .urlQueryItem {
            var components = URLComponents(string: url)!
            
            components.queryItems = parameters?.map {
                
                if let digit = $0.1 as? Int {
                    return URLQueryItem(name: $0.0, value: "\(digit)")
                }
                
                if let value = $0.1 as? String {
                    return URLQueryItem(name: $0.0, value: value)
                }
                
                return URLQueryItem(name: $0.0, value: "")
            }
            var localComponent = components
            components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
            request = URLRequest(url: components.url!)
            
        } else {
            
            var urlPath = ""
            
            for item in parameters ?? [:] {
                urlPath += "/\(item.value)"
            }
            var components = URLComponents(string: url)!
            components = URLComponents(string: url + urlPath)!
            request = URLRequest(url: components.url!)
            print(urlPath)
        }
                
        session.dataTask(with: request!) { data, response, error in
            
            if let data = data {
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    
                    if let _ = json as? Any {
                        completionHandler(data, error, response)
                        return
                    }
                    
                } catch _ {
                    completionHandler(nil, error, response)
                }
            } else {
                completionHandler(nil,error,response)
            }
        }.resume()
    }
    
    func sendPostRequest (_ url: String, parameters: [String: Any]?, _ header: [String: String]? ,_ completionHandler: @escaping (_ data: Data?, _ error: Error? , _ response: URLResponse?) -> ()) {
        
        var components = URLComponents(string: url)!
        components.queryItems = parameters?.map {URLQueryItem(name: $0.0, value: $0.1 as? String)}
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var request = URLRequest(url: components.url!)
        parameters?.map {request.setValue($0.0, forHTTPHeaderField: $0.1 as! String)}
                
        let jsonData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        session.uploadTask(with: request, from: jsonData) { data, response, error in
            
            if let data = data {
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    
                    if let _ = json as? [Any] {
                        completionHandler(data, error, response)
                        return
                    }
                    
                } catch let _ {
                    completionHandler(nil, error, response)
                }
            }
        }.resume()
    }
    
    func imageFromServerURL(urlString: String, placeHolderImage:UIImage, _ completion: @escaping (_ image: UIImage) -> ()) {
 
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                return
            }
            completion(UIImage(data: data!)!)
        
        }).resume()
    }
}
