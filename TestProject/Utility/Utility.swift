//
//  Utility.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import UIKit


class Utility {
    
    class func getAppDelegate() -> AppDelegate? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        return appDelegate
    }
    
    class func initialRootViewController () {
        
        let vc = GitUsersViewController()
        let navigationController = UINavigationController()
        navigationController.viewControllers = [vc]
        kApplicationWindow?.bounds = UIScreen.main.bounds
        kApplicationWindow?.rootViewController = navigationController
        kApplicationWindow?.makeKeyAndVisible()
    }
    
    class func showAlert (message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    class func downloadImage (urlString: String, _ completion: @escaping (_ image: Data) -> ()) {
        
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.global().sync(execute: { () -> Void in
              //  let image = UIImage(data: data!)
                completion((data ?? #imageLiteral(resourceName: "profile").pngData()) ?? Data())
            })
            
        }).resume()
    }
    
    class func inversion (image: UIImage, _ completion: @escaping (_ image: UIImage) -> ()) {
        
        let beginImage = CIImage(image: image)
        
        if let filter = CIFilter(name: "CIColorInvert") {
            filter.setValue(beginImage, forKey: kCIInputImageKey)
            completion(UIImage(ciImage: filter.outputImage!))
        }
    }
}
