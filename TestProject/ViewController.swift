//
//  ViewController.swift
//  TestProject
//
//  Created by MAC on 16/11/2021.
//

import UIKit

class UsersViewController: UIViewController {
    
    
    //MARK: - Outlet
    
    
    //MARK: - Variables
    let viewModel = UsersViewModel()
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    //MARK: - Setup
    func setup () {
        self.view.backgroundColor = .red
        
        
    }
    
    
    //MARK: - Actions
    
    
    //MARK: - Private Methods
    func loadData () {
        viewModel.getUsers {
            
        } failure: { (error) in
            Utility.showAlert(message: error?.localizedDescription ?? "", viewController: self)
        }

    }
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(ConnectionsTableViewCell.self, indexPath: indexPath)
        return cell
    }
}
