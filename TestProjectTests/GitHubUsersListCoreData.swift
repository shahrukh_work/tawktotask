//
//  GitHubUsersList.swift
//  TestProjectTests
//
//  Created by MAC on 18/11/2021.
//

import XCTest
import CoreData
@testable import TestProject

class GitHubUsersListCoreData: XCTestCase {
    var coreDataManager = DataHandler()

    
    override class func setUp() {
        super.setUp()
    }
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    
    
    // MARK: Our test cases
    
    /*this test case test for the proper initialization of CoreDataManager class :)*/
    func test_init_coreDataManager(){
        
        let instance = CoreDataManager.sharedInstance
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        XCTAssertNotNil( instance )
    }
    
    /*test if NSPersistentContainer(the actual core data stack) initializes successfully
     */
    func test_coreDataStackInitialization() {
        let coreDataStack = CoreDataManager.sharedInstance.persistentContainer
        
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        XCTAssertNotNil( coreDataStack )
    }
    
    /*This test case inserts a person record*/
    func test_create_person() {
        
        //Given the name & ssn
        var user1 = User()
        user1.login = "test13"
        user1.note = "test note 2"
        user1.isNoteAvailable = true
        user1.avatar_url = "https://avatars.githubusercontent.com/u/9743939?v=4"
        user1.profileImage = UIImage(named: "profile")!
        
        let person1 = coreDataManager.cacheUsers(users: [user1])
        XCTAssertNotNil( person1 )
    }
    
    func test_fetch_all_person() {
        
        let results = coreDataManager.getUsersData()
        XCTAssertEqual(results.count, 1)
    }
    
    func test_update_person(){
      
        _ = coreDataManager.updateCacheUsers(userId:"test13", note: "hello world")
        /*fetch all items*/
        let itemsFetched = coreDataManager.getUsersData()
        /*get first item*/
        XCTAssertEqual("hello world", itemsFetched.last?.note)
      }
      
}
