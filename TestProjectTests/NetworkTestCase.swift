//
//  NetworkTestCase.swift
//  TestProjectTests
//
//  Created by MAC on 18/11/2021.
//

import XCTest
import CoreData
@testable import TestProject

class NetworkTestCase: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func test_init_api(){
        let vm = GitUsersViewModel()
        var users = [User]()
        let promise = expectation(description: "Status Code: 200")
        
        vm.getUsers(offset: 0) {
            users = vm.users
            XCTAssertGreaterThan(users.count, 0)
            promise.fulfill()
            
        } failure: { (error) in
            users = vm.users
            XCTAssertGreaterThan(users.count, 0)
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let _ = error {
                XCTFail("Timeout")
            }
        }
    }
    
    func test_init_detailapi(){
        var user = User()
        user.login = "tawk"
        let vm = ProfileViewModel(user: user)
    
        let promise = expectation(description: "Status Code: 200")
        
        vm.getUser(userName:vm.user?.login ?? "") {
            XCTAssertEqual(vm.user?.login ?? "", "tawk")
            promise.fulfill()
            
        } failure: { (error) in
            XCTAssertEqual(vm.user?.login ?? "", "tawk")
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 10) { error in
            if let _ = error {
                XCTFail("Timeout")
            }
        }
    }
}
